---
title: "Linear models: All sites"
author: "labrnth"
date: "August, 2019"
output: 
  html_document: 
    number_sections: yes
    code_folding: hide
    theme: readable
    toc: true
    toc_depth: 2
    toc_float: true
editor_options: 
  chunk_output_type: console
---

```{r setupKnitr, include=FALSE}
# `root.dir` sets the knitr working directory to the one that Rstudio
# uses when running comands in the console.
# This allows users to run parts of the Rmarkdown file interactively
# without file location problems.
knitr::opts_knit$set(echo = TRUE, warning = TRUE, collapse = TRUE,
                     root.dir = "../")
# Uncomment the following line to enable chunk caching.
#knitr::opts_chunk$set(fig.width = 8, fig.asp = .4, cache = FALSE)
knitr::opts_chunk$set(cache = TRUE)
```

This worksheet leans heavily on `tidyverse` packages.

```{r setupEnv, include=FALSE}
# Load dependencies and setup environment
source("R/utilities.R")
pkgTest("devtools", "tidyverse", "fs", "sf", "lubridate", "viridis", 
        "raster", "leaflet", "kableExtra", "DT",
        "corrplot", "ellipse", "ggcorrplot", "GGally", "skimr")
#devtools::install_github("ropensci/skimr", ref = "v2")

ggplot2::theme_set(theme_bw(base_size = 12, base_family = "Times New Roman"))
```

# Enumerate inputs
First, enumerate the scaled input files.
```{r}
plot_title <- "April 2019"
output.dir <- "output/"

combined.files <- tibble(abs_path = fs::dir_ls(output.dir, recurse = TRUE, 
                                             glob = "*.jpg|.tif|.rds", 
                                             ignore.case = TRUE)) %>%
  mutate(name = path_ext_remove(path_file(abs_path))) %>%
  mutate(site = str_extract(abs_path, "COS|MOK")) %>%
  mutate(source = str_extract(abs_path, "p4d|sonde|combined")) %>%
  mutate(type = str_extract(name, "nir|red edge|red|green|template")) %>%
  mutate(method = str_extract(name, "mean|median")) %>%
  mutate(scale = str_extract(name, "[^_]+m$")) %>%
  mutate(ext = path_ext(path_file(abs_path))) %>%
  mutate_at(c("source", "type", "method", "scale", "ext"), factor)
```

# Extract raster values {.tabset}
Here, we also calculate some band indices. Note that if the band indices are 
simply a linear combination of the individual band values, they should not offer
any additional explanatory ability over the linear models.

## 1-meter GSD, mean-aggregation
```{r}
sonde.mean.1m <- combined.files %>%
  filter(scale == "1m", source == "combined", method == "mean", ext == "rds") %>%
  pull(abs_path) %>%
  map(~ read_rds(.)) %>%
  reduce(rbind)

# Create alternative band indicies
sonde.mean.1m <- sonde.mean.1m %>%
  mutate(gr_red = green/red, nir_red = nir/red)

# Discard derivative parameters and spatial information
sonde.mean.1m <- sonde.mean.1m %>%
  dplyr::select(temperature, oxygen, turbidity, pH, pycocyanin, chlorophyll, conductivity, 
                green, red, rededge, nir, gr_red, nir_red) %>%
  st_set_geometry(NULL)
```

## 10-meter GSD, mean-aggregation
```{r}
sonde.mean.10m <- combined.files %>%
  filter(scale == "10m", source == "combined", method == "mean", ext == "rds") %>%
  pull(abs_path) %>%
  map(~ read_rds(.)) %>%
  reduce(rbind)

sonde.mean.10m <- sonde.mean.10m %>%
  mutate(gr_red = green/red, nir_red = nir/red)

# Discard derivative parameters and spatial information
sonde.mean.10m <- sonde.mean.10m %>%
  dplyr::select(temperature, oxygen, turbidity, pH, pycocyanin, chlorophyll, conductivity, 
                green, red, rededge, nir, gr_red, nir_red) %>%
  st_set_geometry(NULL)
```

## 1-meter GSD, median-aggregation
```{r}
sonde.median.1m <- combined.files %>%
  filter(scale == "1m", source == "combined", method == "median", ext == "rds") %>%
  pull(abs_path) %>%
  map(~ read_rds(.)) %>%
  reduce(rbind)

# Create alternative band indicies
sonde.median.1m <- sonde.median.1m %>%
  mutate(gr_red = green/red, nir_red = nir/red)

# Discard derivative parameters and spatial information
sonde.median.1m <- sonde.median.1m %>%
  dplyr::select(temperature, oxygen, turbidity, pH, pycocyanin, chlorophyll, conductivity, 
                green, red, rededge, nir, gr_red, nir_red) %>%
  st_set_geometry(NULL)
```

## 10-meter GSD, median-aggregation
```{r}
sonde.median.10m <- combined.files %>%
  filter(scale == "10m", source == "combined", method == "median", ext == "rds") %>%
  pull(abs_path) %>%
  map(~ read_rds(.)) %>%
  reduce(rbind)

sonde.median.10m <- sonde.median.10m %>%
  mutate(gr_red = green/red, nir_red = nir/red)

# Discard derivative parameters and spatial information
sonde.median.10m <- sonde.median.10m %>%
  dplyr::select(temperature, oxygen, turbidity, pH, pycocyanin, chlorophyll, conductivity, 
                green, red, rededge, nir, gr_red, nir_red) %>%
  st_set_geometry(NULL)
```

# Statistics (mean)
## Summary Statistics {.tabset}

### 1-meter GSD
```{r}
plot_title <- "April 2019, 1-meter GSD by mean value"

skim_plus <- skim_with(numeric = sfl(distinct = n_distinct, iqr = IQR), append = TRUE)
skim_plus(sonde.mean.1m)

sonde.mean.1m %>%
  gather(factor_key = TRUE) %>%
  ggplot(aes(x=value)) +
    geom_histogram(bins = 30) + 
    facet_wrap(~key, scales = 'free') + 
  labs(title = plot_title)
```

From the summary statistics, we will omit the following observations:
  * Remove `pH` due to the low spread
  * Remove obvious outliers from `turbidity`
We should also discard `conductivity` measurements due to the small spread, but
the values are technically within the sensing accuracy and could be valid
observations.

```{r}
sonde.mean.1m <- sonde.mean.1m %>%
  dplyr::select(-pH) %>%
  dplyr::mutate(turbidity = remove_outliers_IQR(turbidity)) %>%
  tidyr::drop_na()
```

### 10-meter GSD
```{r}
plot_title <- "April 2019, 10-meter GSD by mean value"

skim_plus <- skim_with(numeric = sfl(distinct = n_distinct, iqr = IQR), append = TRUE)
skim_plus(sonde.mean.10m)

sonde.mean.10m %>%
  gather(factor_key = TRUE) %>%
  ggplot(aes(x=value)) +
    geom_histogram(bins = 30) + 
    facet_wrap(~key, scales = 'free') + 
  labs(title = plot_title)
```

From the summary statistics, we will omit the following observations:
  * Remove `pH` due to the low spread
  * Remove obvious outliers from `turbidity`
We should also discard `conductivity` measurements due to the small spread, but
the values are technically within the sensing accuracy and could be valid
observations.

```{r}
sonde.mean.10m <- sonde.mean.10m %>%
  dplyr::select(-pH) %>%
  dplyr::mutate(turbidity = remove_outliers_IQR(turbidity)) %>%
  tidyr::drop_na()
```


## Scatterplot matrix {.tabset}

### 1-meter GSD
```{r}
plot_title <- "April 2019, 1-meter GSD by mean value"
ggpairs(sonde.mean.1m) + 
  labs(title = plot_title)
```

### 10-meter GSD
```{r}
plot_title <- "April 2019, 10-meter GSD by mean value"
ggpairs(sonde.mean.10m) + 
  labs(title = plot_title)
```

## Correlation  matrix {.tabset}

### 1-meter GSD
```{r}
plot_title <- "April 2019, 1-meter GSD by mean value"
sonde.mean.1m.corr <- cor(sonde.mean.1m)
sonde.mean.1m.corrp <- cor_pmat(sonde.mean.1m)
ggcorrplot(sonde.mean.1m.corr,
    type = "lower", p.mat = sonde.mean.1m.corrp) + 
  labs(title = plot_title)
```

### 10-meter GSD
```{r}
plot_title <- "April 2019, 10-meter GSD by mean value"
sonde.mean.10m.corr <- cor(sonde.mean.10m)
sonde.mean.10m.corrp <- cor_pmat(sonde.mean.10m)
ggcorrplot(sonde.mean.10m.corr,
    type = "lower", p.mat = sonde.mean.10m.corrp) + 
  labs(title = plot_title)
```

# Predictions
## Stepwise Linear Models, comprehensive {.tabset}

### 1-meter GSD
```{r, warning=FALSE}
regressors <- c("green", "red", "rededge", "nir", "gr_red", "nir_red")
regressands <- c("temperature", "oxygen", "turbidity", "pycocyanin", "chlorophyll")

sonde.mean.1m.rtable <- linear_sweep(input.table = sonde.mean.1m,
                                            regressors = regressors,
                                            regressands = regressands)
sonde.mean.1m.rtable %>%
  kable() %>%
  kable_styling() %>%
  scroll_box(width = "1000px", height = "500px")
```

### 10-meter GSD
```{r, warning=FALSE}
regressors <- c("green", "red", "rededge", "nir", "gr_red", "nir_red")
regressands <- c("temperature", "oxygen", "turbidity", "pycocyanin", "chlorophyll")

sonde.mean.10m.rtable <- linear_sweep(input.table = sonde.mean.10m,
                                            regressors = regressors,
                                            regressands = regressands)
sonde.mean.10m.rtable %>%
  kable() %>%
  kable_styling() %>%
  scroll_box(width = "1000px", height = "500px")
```

## Stepwise Linear Models  {.tabset}

### By top $R^2$ {.tabset}

#### 1-meter GSD
```{r}
sonde.mean.1m.rtable %>%
  group_by(regressand) %>%
  filter(R2 == max(R2)) %>%
  kable() %>%
  kable_styling() %>%
  scroll_box(width = "1000px", height = "500px")
```

#### 10-meter GSD
```{r}
sonde.mean.10m.rtable %>%
  group_by(regressand) %>%
  filter(R2 == max(R2)) %>%
  kable() %>%
  kable_styling() %>%
  scroll_box(width = "1000px", height = "500px")
```

### By min $AIC$ {.tabset}

#### 1-meter GSD
```{r}
sonde.mean.1m.rtable  %>%
  group_by(regressand) %>%
  filter(AIC == min(AIC)) %>%
  kable() %>%
  kable_styling() %>%
  scroll_box(width = "1000px", height = "500px")
```

#### 10-meter GSD
```{r}
sonde.mean.10m.rtable  %>%
  group_by(regressand) %>%
  filter(AIC == min(AIC)) %>%
  kable() %>%
  kable_styling() %>%
  scroll_box(width = "1000px", height = "500px")
```

## Single-predictor models {.tabset}

### By top $R^2$ {.tabset}

#### 1-meter GSD
```{r}
sonde.mean.1m.rtable %>%
  filter(NoOfCoef == 1) %>%
  group_by(regressand) %>%
  filter(R2 == max(R2)) %>%
  kable() %>%
  kable_styling() %>%
  scroll_box(width = "1000px", height = "500px")
```

#### 10-meter GSD
```{r}
sonde.mean.10m.rtable %>%
  filter(NoOfCoef == 1) %>%
  group_by(regressand) %>%
  filter(R2 == max(R2)) %>%
  kable() %>%
  kable_styling() %>%
  scroll_box(width = "1000px", height = "500px")
```

### By min $AIC$ {.tabset}

#### 1-meter GSD
```{r}
sonde.mean.1m.rtable %>%
  filter(NoOfCoef == 1) %>%
  group_by(regressand) %>%
  filter(AIC == min(AIC)) %>%
  kable() %>%
  kable_styling() %>%
  scroll_box(width = "1000px", height = "500px")
```

#### 10-meter GSD
```{r}
sonde.mean.10m.rtable %>%
  filter(NoOfCoef == 1) %>%
  group_by(regressand) %>%
  filter(AIC == min(AIC)) %>%
  kable() %>%
  kable_styling() %>%
  scroll_box(width = "1000px", height = "500px")
```

# Statistics (median)
## Summary Statistics {.tabset}

### 1-meter GSD
```{r}
plot_title <- "April 2019, 1-meter GSD by mean value"

skim_plus <- skim_with(numeric = sfl(distinct = n_distinct, iqr = IQR), append = TRUE)
skim_plus(sonde.median.1m)

sonde.median.1m %>%
  gather(factor_key = TRUE) %>%
  ggplot(aes(x=value)) +
    geom_histogram(bins = 30) + 
    facet_wrap(~key, scales = 'free') + 
  labs(title = plot_title)
```

From the summary statistics, we will omit the following observations:
  * Remove `pH` due to the low spread
  * Remove obvious outliers from `turbidity`
We should also discard `conductivity` measurements due to the small spread, but
the values are technically within the sensing accuracy and could be valid
observations.

```{r}
sonde.median.1m <- sonde.median.1m %>%
  dplyr::select(-pH) %>%
  dplyr::mutate(turbidity = remove_outliers_IQR(turbidity)) %>%
  tidyr::drop_na()
```

### 10-meter GSD
```{r}
plot_title <- "April 2019, 10-meter GSD by mean value"

skim_plus <- skim_with(numeric = sfl(distinct = n_distinct, iqr = IQR), append = TRUE)
skim_plus(sonde.median.10m)

sonde.median.10m %>%
  gather(factor_key = TRUE) %>%
  ggplot(aes(x=value)) +
    geom_histogram(bins = 30) + 
    facet_wrap(~key, scales = 'free') + 
  labs(title = plot_title)
```

From the summary statistics, we will omit the following observations:
  * Remove `pH` due to the low spread
  * Remove obvious outliers from `turbidity`
We should also discard `conductivity` measurements due to the small spread, but
the values are technically within the sensing accuracy and could be valid
observations.

```{r}
sonde.median.10m <- sonde.median.10m %>%
  dplyr::select(-pH) %>%
  dplyr::mutate(turbidity = remove_outliers_IQR(turbidity)) %>%
  tidyr::drop_na()
```


## Scatterplot matrix {.tabset}

### 1-meter GSD
```{r}
plot_title <- "April 2019, 1-meter GSD by mean value"
ggpairs(sonde.median.1m) + 
  labs(title = plot_title)
```

### 10-meter GSD
```{r}
plot_title <- "April 2019, 10-meter GSD by mean value"
ggpairs(sonde.median.10m) + 
  labs(title = plot_title)
```

## Correlation  matrix {.tabset}

### 1-meter GSD
```{r}
plot_title <- "April 2019, 1-meter GSD by mean value"
sonde.median.1m.corr <- cor(sonde.median.1m)
sonde.median.1m.corrp <- cor_pmat(sonde.median.1m)
ggcorrplot(sonde.median.1m.corr,
    type = "lower", p.mat = sonde.median.1m.corrp) + 
  labs(title = plot_title)
```

### 10-meter GSD
```{r}
plot_title <- "April 2019, 10-meter GSD by mean value"
sonde.median.10m.corr <- cor(sonde.median.10m)
sonde.median.10m.corrp <- cor_pmat(sonde.median.10m)
ggcorrplot(sonde.median.10m.corr,
    type = "lower", p.mat = sonde.median.10m.corrp) + 
  labs(title = plot_title)
```

# Predictions
## Stepwise Linear Models, comprehensive {.tabset}

### 1-meter GSD
```{r, warning=FALSE}
regressors <- c("green", "red", "rededge", "nir", "gr_red", "nir_red")
regressands <- c("temperature", "oxygen", "turbidity", "pycocyanin", "chlorophyll")

sonde.median.1m.rtable <- linear_sweep(input.table = sonde.median.1m,
                                            regressors = regressors,
                                            regressands = regressands)
sonde.median.1m.rtable %>%
  kable() %>%
  kable_styling() %>%
  scroll_box(width = "1000px", height = "500px")
```

### 10-meter GSD
```{r, warning=FALSE}
regressors <- c("green", "red", "rededge", "nir", "gr_red", "nir_red")
regressands <- c("temperature", "oxygen", "turbidity", "pycocyanin", "chlorophyll")

sonde.median.10m.rtable <- linear_sweep(input.table = sonde.median.10m,
                                            regressors = regressors,
                                            regressands = regressands)
sonde.median.10m.rtable %>%
  kable() %>%
  kable_styling() %>%
  scroll_box(width = "1000px", height = "500px")
```

## Stepwise Linear Models  {.tabset}

### By top $R^2$ {.tabset}

#### 1-meter GSD
```{r}
sonde.median.1m.rtable %>%
  group_by(regressand) %>%
  filter(R2 == max(R2)) %>%
  kable() %>%
  kable_styling() %>%
  scroll_box(width = "1000px", height = "500px")
```

#### 10-meter GSD
```{r}
sonde.median.10m.rtable %>%
  group_by(regressand) %>%
  filter(R2 == max(R2)) %>%
  kable() %>%
  kable_styling() %>%
  scroll_box(width = "1000px", height = "500px")
```

### By min $AIC$ {.tabset}

#### 1-meter GSD
```{r}
sonde.median.1m.rtable  %>%
  group_by(regressand) %>%
  filter(AIC == min(AIC)) %>%
  kable() %>%
  kable_styling() %>%
  scroll_box(width = "1000px", height = "500px")
```

#### 10-meter GSD
```{r}
sonde.median.10m.rtable  %>%
  group_by(regressand) %>%
  filter(AIC == min(AIC)) %>%
  kable() %>%
  kable_styling() %>%
  scroll_box(width = "1000px", height = "500px")
```

## Single-predictor models {.tabset}

### By top $R^2$ {.tabset}

#### 1-meter GSD
```{r}
sonde.median.1m.rtable %>%
  filter(NoOfCoef == 1) %>%
  group_by(regressand) %>%
  filter(R2 == max(R2)) %>%
  kable() %>%
  kable_styling() %>%
  scroll_box(width = "1000px", height = "500px")
```

#### 10-meter GSD
```{r}
sonde.median.10m.rtable %>%
  filter(NoOfCoef == 1) %>%
  group_by(regressand) %>%
  filter(R2 == max(R2)) %>%
  kable() %>%
  kable_styling() %>%
  scroll_box(width = "1000px", height = "500px")
```

### By min $AIC$ {.tabset}

#### 1-meter GSD
```{r}
sonde.median.1m.rtable %>%
  filter(NoOfCoef == 1) %>%
  group_by(regressand) %>%
  filter(AIC == min(AIC)) %>%
  kable() %>%
  kable_styling() %>%
  scroll_box(width = "1000px", height = "500px")
```

#### 10-meter GSD
```{r}
sonde.median.10m.rtable %>%
  filter(NoOfCoef == 1) %>%
  group_by(regressand) %>%
  filter(AIC == min(AIC)) %>%
  kable() %>%
  kable_styling() %>%
  scroll_box(width = "1000px", height = "500px")
```


# Tasks
* DRY
* Create a function `get_path` that wraps `raster::extract(pull(filter(...), abs_path))`
* Create a function to perform the raster value extraction or vectorize the 
workflow.