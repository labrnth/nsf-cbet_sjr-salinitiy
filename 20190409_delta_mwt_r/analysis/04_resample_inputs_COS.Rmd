---
title: "Resample Inputs: COS"
author: "labrnth"
date: "August, 2019"
output: 
  html_document: 
    number_sections: yes
    code_folding: hide
    theme: readable
    toc: true
    toc_depth: 2
    toc_float: true
editor_options: 
  chunk_output_type: console
---

```{r setupKnitr, include=FALSE}
# `root.dir` sets the knitr working directory to the one that Rstudio
# uses when running comands in the console.
# This allows users to run parts of the Rmarkdown file interactively
# without file location problems.
knitr::opts_knit$set(echo = TRUE, warning = TRUE, collapse = TRUE)
# Uncomment the following line to enable chunk caching.
#knitr::opts_chunk$set(fig.width = 8, fig.asp = .4, cache = FALSE)
knitr::opts_chunk$set(cache = TRUE)
```

This worksheet leans heavily on `tidyverse` packages. 

This worksheet also uses an odd combination of `raster`+`sf`+ `stars` functions. 
Because of this there are many namespace collisions. I default to being overly-explicit 
with the `::` operator (while being aware of the [performance penalty][1].

TODO: It is worth evaluating where `stars` can improve package cohesion (or not).

```{r setupEnv, include=FALSE}
# Load dependencies and setup environment
source("R/utilities.R")
pkgTest("devtools", "raster", "tidyverse", "fs", "sf", "stars", "lubridate",
        "viridis", "leaflet", "kableExtra", "DT",
        "corrplot", "ellipse", "ggcorrplot", "GGally", "skimr")
devtools::install_github("ropensci/skimr", ref = "v2")

ggplot2::theme_set(theme_bw(base_size = 12, base_family = "Times New Roman"))
```

```{r importData}
plot_title <- "April 2019, COS"
input.dir <- "input/p4d_MWT_20190409_COS_MS_02/reflectance/"
output.dir <- "output/COS/"

input.files <- tibble::tibble(abs_path = fs::dir_ls(input.dir, recursive = TRUE,
                                                    glob = "*.jpg|.tif",
                                                    ignore.case = TRUE)) %>%
  dplyr::mutate(name = path_ext_remove(path_file(abs_path))) %>%
  dplyr::mutate(type = str_extract(name, "nir|red edge|red|green")) %>%
  dplyr::mutate_at(c("type"), factor)
```

```{r}

grn <- raster("input/p4d_MWT_20190409_COS_MS_02/reflectance/COS_MS_02_transparent_reflectance_green.tif")
red <- raster("input/p4d_MWT_20190409_COS_MS_02/reflectance/COS_MS_02_transparent_reflectance_red.tif")
nir <- raster("input/p4d_MWT_20190409_COS_MS_02/reflectance/COS_MS_02_transparent_reflectance_nir.tif")
reg <- raster("input/p4d_MWT_20190409_COS_MS_02/reflectance/COS_MS_02_transparent_reflectance_red edge.tif")

sonde <- read_csv("input/sonde_190409_cleaned.csv") %>%
  dplyr::filter(Site == "COS") %>%
  dplyr::rename(temperature = TEMP.C,
         pressure = PRESS.mmHg,
         oxygen = DO.mgL,
         conductivity = C.mScm,
         turbidity = NTU,
         pycocyanin = BGA.PC.ugL,
         chlorophyll = Chl.ugL,
         depth = DEP.m) %>%
  dplyr::filter(Time < hms("10:16:41")) %>%
  readr::write_csv("output/sonde_190409_COS_cleaned.csv")
  

sonde_sf <- sf::st_as_sf(sonde, coords = c('Lon', 'Lat'), agr = "constant", crs = "+init=epsg:4326") %>%
  st_transform(32610)

sonde_coords <- sf::st_coordinates(sonde_sf)
```

# Image scaling
First, check current ground sampliing distance for each channel:
```{r}
res(grn)
res(red)
res(nir)
res(reg)
```
The GSD is a little over 10 cm (0.10451 meters) *but* not quite the same as the
other site for this day (0.10571 meters).Map units are in meters for the 
UTM CRS.

Now, we scale images to a GSD of: 1, 5, 10, 15, 20 and 30 meters. 
This requires aggregation factors of 10, 50, 100, 150, 200, and 300 respectively.

Using `raster::aggregate`:
```{r}
# Note that ~aggregate~ has a bug and fails with overwrite=FALSE, complaining 
# complaining that the file exists...
# WARNING: sys.call() is used to parse the name of the function
# supplied as an imput argument (specifically, `sumstat`)
aggregate_byfactor <- function(raster_in, scalefactor, outfolder, name, type, sumstat) {
  fs::dir_create(paste0(outfolder, "p4d_scaled/"), 
             recursive = TRUE)
  outpath <- paste0(outfolder, "p4d_scaled/", name, "_", as.character(sys.call())[7],
                    "_", scalefactor/10, "m", ".tif")
  raster::aggregate(raster(raster_in), fact = scalefactor, fun = sumstat,
                   expand = FALSE, na.rm = FALSE, overwrite = TRUE,
                   filename = outpath)
}
```

TODO: Vectorize
NOTE: Make sure you have at least 6GB of memory free for this process.
Sometimes R takes a lot of memory, sometimes it doesn't (for the same operation
no less)

Now, perform the aggregations to a GSD of 1, 5, 10, 15, 20 and 30 meters:
```{r}
for (gsd in c(1, 5, 10, 15, 20, 30)) {
  scalefactor <- gsd * 10
  # WARNING: hardcoded row indicies
  for (row in 1:nrow(input.files)) {
    aggregate_byfactor(input.files[[row,"abs_path"]], scalefactor, output.dir, 
                       input.files[[row,"name"]], input.files[[row,"type"]],
                       mean)
    aggregate_byfactor(input.files[[row,"abs_path"]], scalefactor, output.dir, 
                       input.files[[row,"name"]], input.files[[row,"type"]],
                       median)
  }
  # WARNING: hardcoded row
  row <- 1
  raster::writeRaster(raster(raster::aggregate(raster(input.files[[row,"abs_path"]]),
                                               fact = scalefactor, fun = max,
                                               expand = FALSE, na.rm = FALSE)),
                      filename = paste0(output.dir, "p4d_scaled/", "template", "_", 
                                        scalefactor/10, "m", ".tif"),
                      overwrite = TRUE)
}
```


```{r}
scaled.files <- tibble(abs_path = fs::dir_ls(output.dir, recurse = TRUE, 
                                             glob = "*.jpg|.tif|.rds", 
                                             ignore.case = TRUE)) %>%
  mutate(name = path_ext_remove(path_file(abs_path))) %>%
  mutate(source = str_extract(abs_path, "p4d|sonde")) %>%
  mutate(type = str_extract(name, "nir|red edge|red|green|template")) %>%
  mutate(method = str_extract(name, "mean|median")) %>%
  mutate(scale = str_extract(name, "[^_]+m$")) %>%
  mutate(ext = path_ext(path_file(abs_path))) %>%
  mutate_at(c("source", "type", "method", "scale", "ext"), factor)
```

# Sonde resampling
Now, we will aggregate the sonde observations within a raster (grid) cell at each
respective GSD.

Using `raster::rasterize`:
```{r}
scaled.templates <- scaled.files %>%
  dplyr::filter(type == "template")

sonde.sp <- sonde_sf %>%
  dplyr::select(-Date, -Time, -Site) %>%
  sf::as_Spatial()

output.dir.sonde <- fs::dir_create(paste0(output.dir, "sonde_scaled/"),
                                   recursive = TRUE)

sonde.spatialnames <- sonde.sp %>%
  names() %>%
  readr::write_rds(paste0(output.dir.sonde,"/layernames.rds"))

for (row in 1:nrow(scaled.templates)) {
  template <- raster(scaled.templates[[row,"abs_path"]])

  ra.out <- raster::rasterize(sonde.sp, template, 
              field = sonde.spatialnames, overwrite = TRUE,
              fun = function(x,...)mean(x, na.rm = TRUE),
              filename = paste0(output.dir, "sonde_scaled/COS_mean_",
                                scaled.templates[[row,"scale"]], ".tif"))
  sp.out <- rasterToPoints(ra.out, spatial=TRUE)
  names(sp.out) <- sonde.spatialnames
  sf.out <- sf::st_as_sf(sp.out) %>%
    write_rds(paste0(output.dir, "sonde_scaled/COS_mean_",
                                scaled.templates[[row,"scale"]], ".rds"))
  
  ra.out <- raster::rasterize(sonde.sp, template, 
              field = sonde.spatialnames, overwrite = TRUE,
              fun = function(x,...)median(x, na.rm = TRUE),
              filename = paste0(output.dir, "sonde_scaled/COS_median_",
                                scaled.templates[[row,"scale"]], ".tif"))
  sp.out <- rasterToPoints(ra.out, spatial=TRUE)
  names(sp.out) <- sonde.spatialnames
  sf.out <- sf::st_as_sf(sp.out) %>%
    write_rds(paste0(output.dir, "sonde_scaled/COS_median_",
                                scaled.templates[[row,"scale"]], ".rds"))
}
```

Note that `mean` and `median` may complain and return warnings when attempting to compute
summary statistics for empty cells. I believe `mask = TRUE` is supposed to skip
calculations for cells that don't overlap the point (read: vector) features, however
I was not able to get it to produce a reasonable output. This could probably be
solved by masking the template raster first, by the spatial features.
*TODO:* For now, the operation is quick enough, but for large rasters, it may take 
a lot of time.

Using `stars` is not possible yet, since `st_rasterize` doesn't quite match `rasterize` in terms of feature parity (or I have yet figured out the gdal
equivalents). The raster-to-vector functions from `stars` could used, since
the objective is to get back to a `sf` data frame, however the handling of
names is more complete in `sp`+`raster`, so those fuctions are used.

# Raster resampling
Now, we will re-sample the scaled rasters according to the sonde locations.
This information is stored in a data frame and saved to disk for retrieval later
for the predictive models.

## Extract raster values
```{r}
output.dir.extracted <- fs::dir_create(paste0(output.dir, "combined_extracted"),
                                   recurse = TRUE)

scaled.files <- scaled.files %>%
  filter(!is.na(method))

for (i.method in unique(scaled.files$method)) {
  for (i.scale in unique(scaled.files$scale)) {
   filtered.files <- scaled.files %>%
     filter(scale == i.scale, method == i.method)
   
   sonde <- filtered.files %>%
     filter(source == "sonde", ext == "rds") %>%
     pull(abs_path) %>%
     read_rds
   
   sonde.plus.extracted <- sonde %>%
     mutate(green = raster::extract(raster(pull(filter(filtered.files, type == "green"), abs_path)),
                                   st_coordinates(sonde), method = "simple"),
         rededge = raster::extract(raster(pull(filter(filtered.files, type == "red edge"), abs_path)),
                                   st_coordinates(sonde), method = "simple"),
         red = raster::extract(raster(pull(filter(filtered.files, type == "red"), abs_path)),
                               st_coordinates(sonde), method = "simple"),
         nir = raster::extract(raster(pull(filter(filtered.files, type == "nir"), abs_path)),
                               st_coordinates(sonde), method = "simple")) %>%
     write_rds(path = paste0(output.dir.extracted, "/COS_", i.method, "_", i.scale, ".rds"))
   }
}
```


# Footnotes
[1]: https://stat.ethz.ch/pipermail/r-devel/2017-September/074850.html)
> Many people seem to forget that every use of `::` is an R
> function call and using it is ineffecient compared to just using
> the already imported name.